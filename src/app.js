import * as d3         from 'd3'
import {demo1}         from './demo/demo1'
import {demo2}         from './demo/demo2'
import {demo3}         from './demo/demo3'
import {demo4}         from './demo/demo4'

const g1         =  `digraph  {  layout="twopi"  a -> b ;node[style=invis] edge[style=invis] b-> c ; b -> d ; b -> e ; b -> f ; b-> g ;   }`;
const g2         =  `digraph  {  layout="twopi"  a -> b ; b-> c ; node[style=invis] edge[style=invis] b -> d   b -> e ; b -> f ; b-> g    }`;
const g3         =  `digraph  {  layout="twopi"  a -> b ; b-> c ; b -> d ; node[style=invis] edge[style=invis]  b -> e ; b -> f ; b-> g   }`;
const g4         =  `digraph  {  layout="twopi"  a -> b -> c ; b -> d ; b -> e ; node[style=invis] edge[style=invis]  b -> f  ; b-> g     }`;
const g5         =  `digraph  {  layout="twopi"  a -> b -> c ; b -> d ; b -> e ; b -> f ;node[style=invis] edge[style=invis] b-> g        }`;
const g6         =  `digraph  {  layout="twopi"  a -> b -> c ; b -> d ; b -> e ; b -> f ; b-> g  node[style=invis] edge[style=invis]      }`;




const gv1  = `digraph  {
    layout="dot"   
     a[shape="circle"  image="http://localhost:3018/icons/icon1.svg" label=""] ;  
 }`;
const gv2 = `digraph  {  
     layout="dot"   
     a[ shape="circle"  image="dist/icons/icon1.svg" label=""] ;  
     a -> b
     b[ shape="circle"  image="dist/icons/icon2.svg" label=""] ;
 }`;
const graphs = [gv1,gv2]
window.d3 = d3
window.onload = function (){
    demo1("#graph1")
  /*  const demos = [
        {path:"./svg/step1.svg",title:"graphviz step1"},
        {path:"./svg/step2.svg",title:"graphviz step2"},
       ]
       const container =  document.querySelector("#graph2")
       let lastGraph ; 
       document.addEventListener("keydown",(e)=>{
           // get the existing svg node
           // maybe clear it ?
           // get the svg target
           // display it 
           // make the transition ( if i am here )

           if(e.code ==="ArrowRight"){    
               const node = getGraphic(container,demos[1])
               node.then(x=>{           
                   container.removeChild(lastGraph);
                   lastGraph = x;
                   container.appendChild(x);
                //d3.select("#graph2 svg").
               })            
           }
           if(e.code ==="ArrowLeft"){      
            const node = getGraphic(container,demos[0])
            node.then(x=>{           
                container.removeChild(lastGraph);
                lastGraph = x;
                container.appendChild(x);
             //d3.select("#graph2 svg").
            })            
        }
       })
       getGraphic(container,demos[0]).then((x)=> {
           lastGraph = x
           container.appendChild(x)
        })*/
}

function svgDraw(){
    var svg = d3.select("body")
    .append('svg')
    .attr('width', '375px')
    .attr('height', '490px');


svg.append('path').attr('d', d_t0);
svg.selectAll('path').transition()
               .duration(3500).delay(1000)
               .attr('d', d);
}
function createContainer(){
    const container =  document.querySelector(".container")
    const div       =  document.createElement("div")
    div.setAttribute("class","graph")
    container.appendChild(div)
    return div
}
 async function getGraphic (container,{title,path}){
    console.log(`getting ${path}`)     
    const node = (await d3.svg(path)).querySelector("svg")    
    const images      =  [...node.querySelectorAll("image")]
    console.log(`number of images found ${path} : ${images.length}`)      
    images.map(image=>{
       const hrefAttribute  =   image.getAttribute('xlink:href')   
       const href =  hrefAttribute.replace("dist","")     
       console.log(`hrefAttribute ${href}`)
       image.setAttribute('xlink:href',href)
      })        
   // const header       =  document.createElement("h2")
   // header.textContent = title
  //  container.setAttribute("class","graph")
  //  container.style.width = node.width.baseVal.valueAsString;
   // container.appendChild(header)
   // container.appendChild(node)
  //  console.log(`${path} added`)
    return node
}