import fs from 'fs'
import path from 'path'
import { opendir } from 'fs/promises';
import {exec,spawn }   from 'child_process'

async function getFiles(path,pattern){
    try {
        const files = [];
        const dir   = await opendir(path);
        for await (const file of dir) file.name.match(pattern) ? files.push(file.name) : undefined;
        return files;
          
      } catch (err) {
       return console.error(err);
      }
}
//dot
//neato
//twopi
//circo
//fdp
//osage
//patchwork
//sfdp
//
export async function executeGraphviz(inputDir,regex){
    inputDir =  inputDir || "./src/graphviz"
    console.log(`${inputDir} changed`)
   regex = /^(step)/
   // regex = regex || /^(city-fede)/
    const pattern =  new RegExp(regex)
    console.log(`root: ${inputDir} regex : ${pattern.source}`)
    const filesName   = await getFiles(inputDir,pattern);
    console.log(`${filesName.length} files found`)
    const outputDir  = "./dist/svg"
    const layout     = "neato"
    const getCommand = (inputDir,fileName,outputDir,outputFileName)=>`dot.exe -K${layout}  -Tsvg  ${inputDir}/${fileName} > ${outputDir}/${outputFileName}`

    const processFile = (cmd)=> exec(cmd, (error, stdout, stderr) => {
        if (error) return  console.error(`exec error: ${error}`);       
        if(stderr)console.error(`stderr: ${stderr}`);
        console.log(`cmd: "${cmd}" executed `)
     });     

     const range = /*[...Array(4).keys()].map(x=>filesName[0]) ||*/ filesName;
     range.map((x,i)=>processFile(getCommand(inputDir,x,outputDir,`${x.split(".")[0]}.svg`)))
}

executeGraphviz()