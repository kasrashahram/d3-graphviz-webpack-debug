
import * as d3             from 'd3'
import {graphviz} from "d3-graphviz"


export function demo2(selector){     
     
    const graph      =  d3.select(selector).graphviz()
    const g1         =  `digraph  {a -> b}`;
    const g2         =  `digraph  {a -> b -> c}`;
    const g3         =  `digraph  {a -> b -> c -> d}`;
    const dots       =  [g1,g2,g3,g2]
    const next       =  (index)=>(index + 1) % dots.length
    const render     =  (index)=> graph.renderDot(dots[index]).on("end", render.bind(null,next(index)));
    const transition =  ()=> d3.transition("demo2").ease(d3.easeLinear).delay(500).duration(500);
    graph.transition(transition).on("initEnd", render.bind(null,0));   

}