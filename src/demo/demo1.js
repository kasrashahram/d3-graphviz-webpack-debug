import * as d3             from 'd3'
export function demo1(selector,graphs=[]){
    let index     = 0;

    const gv1  = `digraph  {
        layout="dot"   
         a[shape="circle"  image="icons/icon1.svg" label=""] ;  
     }`;
    const gv2 = `digraph  {  
         layout="dot"   
         a[ shape="circle"  image="icons/icon1.svg" label=""] ;  
         a -> b
         b[ shape="circle"  image="icons/icon2.svg" label=""] ;
     }`;
    graphs = [gv1,gv2]

    let canChange = true;  
    document.addEventListener('keydown', logKey);
    document.addEventListener('keyup', onKeyup);
    const transition =  () => d3.transition("demo1").ease(d3.easeLinear).delay(100).duration(500);  
    const condition  =  (e)=> (e.code ==="ArrowRight" || e.code ==="ArrowLeft") 
    function onKeyup(e){  
        if(!condition(e))return;
         canChange = true
     }  
    function logKey(e) { 
       if(!condition(e))return;
       if(!canChange)return;
       if(e.code ==="ArrowRight" && index < (graphs.length-1)) index+=1 ;
       if(e.code ==="ArrowLeft"  &&  index > 0)  index-=1 ;
       canChange = false 
       graph.transition(transition).renderDot(graphs[index]);
    }
    const graph     = d3.select(selector).graphviz(selector); 

    graph
    .addImage("icons/icon1.svg","100","100")
    .addImage("icons/icon2.svg","100","100")
    .renderDot(graphs[index]);  
}