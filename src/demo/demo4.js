import * as d3             from 'd3'


export function demo4(selector){
  let index = 0;
  let canChange = true;

  document.addEventListener('keydown', logKey);
  document.addEventListener('keyup', onKeyup);

  function onKeyup(){
      canChange = true
  }

  function logKey(e) { 
    if(!canChange)return
    if(e.code ==="ArrowRight"){
       index++
       console.log(index)
    }
    if(e.code ==="ArrowLeft"){
        index--
        console.log(index)
    }
    updateAll(index)
    canChange = false
  }
    const svg = d3.select("#graph4").append("svg").style("background-color","gray")

    


function getData(key=0) {
	return [...Array(5).keys()].map((x,i)=>i*50 + 10*key)
}

function update(data) {
	d3.select('svg')
		.selectAll('circle')
		.data(data)
		.join('circle')
		.attr('cy', 50)
		.attr('r', 5)
		.transition()
        .ease(d3.easeLinear)
        .duration(200)
		.attr('cx', function(d) {
			return d;
		});
}

function updateAll(key) {    
	update(getData(key));
}

updateAll();
window.updateGraph = updateAll
    
}
