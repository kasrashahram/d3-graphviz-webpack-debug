const path = require("path")

module.exports = {
    entry: ['./src/app.js'],  
    output:{
        filename:"bundle.js",
        path:path.resolve(__dirname,'dist')
    },  

      
    watch:true,
    devServer : {
        port:3018,
       static:path.resolve(__dirname,'dist'),
       
    }, 
    mode:"development",

}
